package website

import com.bjss.hopcroft.david.bjsstest.Navigate
import com.bjss.hopcroft.david.bjsstest.SystemConfiguration
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriverException
import org.openqa.selenium.firefox.FirefoxDriver
import spock.lang.Specification


class WebTestsSpec extends Specification {
    WebDriver driver

    def setup() {
        SystemConfiguration.systemSettings()
        this.driver = new FirefoxDriver()
    }

    /**
     * Smoke test (extra to requirement)
     * Extra test to Verify the WebDriver components are working
     */
    def "Verify a bad URL throws exception"() {
        when:
            driver.get("http://automationpracticexxx.com/")
        then:
            thrown(WebDriverException)
    }

    /**
     * Smoke test (extra to requirement)
     * Extra test to Verify the site to be tested is contactable
     */
    def "I can access the SUT"() {
        given:
            driver.get(System.getProperty("test.data.baseurl"))
        expect:
            driver.title == "My Store"
    }

    def "Purchase two itmes"() {
        given:
            def cartInfo
            Navigate site = new Navigate(driver)
            site.gotoHomePage()
        and:
            def shoppingList = [[
                "plp": "http://automationpractice.com/index.php?id_category=3&controller=category",
                "pdp": "http://automationpractice.com/index.php?id_product=7&controller=product",
                "item": "Printed Chiffon Dress",
                "size": "M",
                "quantity": 1,
                "colour": "Yellow",
                "expectedPrice": "16.40"
            ],[
                "plp": "http://automationpractice.com/index.php?id_category=3&controller=category",
                "pdp": "http://automationpractice.com/index.php?id_product=1&controller=product",
                "item": "Faded Short Sleeve T-shirts",
                "quantity": 1,
                "expectedPrice": "16.51"
            ]]
            def user = ["login":"as@mailinator.com", "password":"BJSSTest"]
        when:
            double price
            price = site.actionBrowsePlpAndAddToCart(shoppingList[0])
            shoppingList[0]."siteprice" = price // Store for later testing
            price = site.actionBrowsePlpAndAddToCart(shoppingList[1])
            shoppingList[1]."siteprice" = price // Store for later testing
            site.gotoCart()
            cartInfo = site.parseCheckout()
            site.gotoCheckout()
            site.payForItems(user)
        then:
            print "cart info ->>> $cartInfo"
            cartInfo["items"][0]["size"] == shoppingList[0]["size"]
        and:
            cartInfo["items"][0]["price"] == shoppingList[0]["expectedPrice"]
        and:
            cartInfo["items"][1]["price"] == shoppingList[1]["expectedPrice"]
        and:
            (new BigDecimal(cartInfo["totalProducts"])) == (new BigDecimal(shoppingList[0]["expectedPrice"])) \
                                                        + (new BigDecimal(shoppingList[1]["expectedPrice"]))
        and:
            (new BigDecimal(cartInfo["totalPrice"])) == (new BigDecimal(shoppingList[0]["expectedPrice"])) \
                                                        + (new BigDecimal(shoppingList[1]["expectedPrice"])) \
                                                        + (new BigDecimal(cartInfo["totalShipping"]))
    }

    def "Review Previous order and add a message"() {
        given:
            def user = ["login":"as@mailinator.com", "password":"BJSSTest"]
            String message = "Deliver A.S.A.P"
            String messageOnOrder
            Navigate site = new Navigate(driver)
        when:
            site.gotoHomePage()
            site.loginToAccount(user)
            site.addMessageToLatestOrder(message)
            messageOnOrder = site.getLatestMessageFromOrder()
            site.signOut()
        then:
            messageOnOrder == message
    }

    def cleanup() {
        this.driver.quit()
    }
}
