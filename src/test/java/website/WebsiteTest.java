package website;

import com.bjss.hopcroft.david.bjsstest.Navigate;
import com.bjss.hopcroft.david.bjsstest.SystemConfiguration;
import com.bjss.hopcroft.david.bjsstest.selenium.support.AutoScreenShotRule;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static com.bjss.hopcroft.david.bjsstest.selenium.support.Waits.waitSeconds;

public class WebsiteTest {
    private static WebDriver driver;

    @ClassRule
    @Rule
    public static AutoScreenShotRule autoScreenShotRule;

    @BeforeClass
    public static void setup(){
        SystemConfiguration.systemSettings();
        driver = new FirefoxDriver();
        autoScreenShotRule = new AutoScreenShotRule(driver);
    }

    @Test
    public void verifyScreenShotOnTestFail() {
        Navigate site = new Navigate(driver);
        site.gotoHomePage();
        waitSeconds(5);
        Assert.fail();
    }

}
