package website

import com.bjss.hopcroft.david.bjsstest.Navigate
import com.bjss.hopcroft.david.bjsstest.SystemConfiguration
import com.bjss.hopcroft.david.bjsstest.selenium.support.Waits
import spock.lang.Ignore
import org.openqa.selenium.WebDriver
import org.openqa.selenium.firefox.FirefoxDriver
import spock.lang.Specification


class CreateSiteUser extends Specification {
    WebDriver driver

    def setup() {
        SystemConfiguration.systemSettings()
        this.driver = new FirefoxDriver()
    }

    @Ignore("Only run this when you want to create a new user")
    def "Create account"(gender, surname, name, email, passwd, dob, mob, yob, news, offer, address, city, state, zip, mobile, alias) {
        given:
            driver.get(System.getProperty("test.data.baseurl"))
            Navigate site = new Navigate(driver)
        when:
            def user = [
                "gender": gender,
                "surname": surname,
                "name": name,
                "email": email,
                "passwd": passwd,
                "dob": dob,
                "mob": mob,
                "yob": yob,
                "news": news,
                "offer": offer,
                "address": address,
                "city": city,
                "state": state,
                "zip": zip,
                "mobile": mobile,
                "alias": alias
            ]
        then:
            site.gotoAccountCreation()
            site.createAccount(user)

        where:
            // Too much data here I should read this from a database/table
            gender | surname | name   | email               | passwd     | dob  | mob    | yob    | news   | offer  | address      | city         | state  | zip     | mobile           | alias
             "Mr"  | "Smith" | "Alan" | "as@mailinator.com" | "BJSSTest" | "1"  | "May " | "1970" | "true" | "true" | "1 High St." | "Des Moines" | "Iowa" | "50320" | "(515) 555-2345" | "Test #1"
    }

    def cleanup() {
        this.driver.quit()
    }
}
