package api

import groovy.json.JsonSlurper
import spock.lang.Specification
import groovyx.net.http.*
import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.Method.DELETE
import static groovyx.net.http.Method.POST
import static groovyx.net.http.Method.PUT

class VerifyApiSpec extends Specification {
    String endpoint = "https://reqres.in/api"

    /**
     * Helper Method to make http-builder calls to the API
     * (Could extend this to be more like python request library)
     *
     * @param method PUT, POST, DELETE
     * @param path path of endpoint
     * @param data data to be
     * @return a map containing response data
     */
    def simpleRequest(method, path, data = "") {
        def result = [:]

        def http = new HTTPBuilder(this.endpoint)
        http.request(method, JSON) {
            uri.path = path
            headers.'User-Agent' = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11'
            result."text" =""
            response.success = { resp, reader ->
                result."status" = resp.status
                result."text" = reader
            }
        }
        return result
    }

    def "Verify create with POST method"() {
        given:
            def res = this.simpleRequest(POST, '/api/users', '{"name": "morpheus", "job": "leader"}')
        expect:
            res.status == 201    // response code is "Created"
        and:
            res.text.id != null  // also confirm that there is some data in response
    }

    def "Verify read with GET method responds OK"() {
        given:
            def http = new URL("${endpoint}/users/2").openConnection()
            http.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11")
        when:
            def getRC = http.responseCode
        then:
            getRC == 200
    }

    def "Verify read with GET method returns expected data values"() {
        given:
            def http = new URL("${endpoint}/users/2").openConnection()
            http.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11")
        when:
            def response = http.inputStream.text
            def result = new JsonSlurper().parseText(response)
        then:
            result.data.id == 2
        and:
            result.data.first_name == "Janet"
    }

    def "verify update with PUT method"() {
        given:
            def res = this.simpleRequest(PUT, '/api/users/2', '{"name": "morpheus", "job": "zion resident"}')
        expect:
            res.status == 200
    }

    def "verify delete with DELETE method"() {
        given:
            def res = this.simpleRequest(DELETE, '/api/users/2')
        expect:
            res.status == 204
    }
}
