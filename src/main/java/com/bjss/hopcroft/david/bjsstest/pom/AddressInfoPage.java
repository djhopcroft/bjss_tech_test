package com.bjss.hopcroft.david.bjsstest.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AddressInfoPage {
    WebDriver driver;

    @FindBy(how = How.NAME, using="processAddress")
    private WebElement toCheckoutButton;

    public AddressInfoPage(WebDriver wd) {
        this.driver = wd;
        PageFactory.initElements(this.driver,this);
    }


    public void clickProceedToCheckout() {
        toCheckoutButton.click();
    }
}
