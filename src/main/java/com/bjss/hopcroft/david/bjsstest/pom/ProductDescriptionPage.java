package com.bjss.hopcroft.david.bjsstest.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ProductDescriptionPage {

    WebDriver driver;

    @FindBy(how = How.ID, using="add_to_cart")
    private WebElement addToCart;

    @FindBy(how = How.ID, using="quantity_wanted")
    private WebElement quantityToOrder;

    @FindBy(how = How.ID, using="our_price_display")
    private WebElement howMuch;

    @FindBy(how = How.XPATH, using="//*[@id=\"quantity_wanted_p\"]/a[1]/span/i")
    private WebElement cartAddOne;

    @FindBy(how = How.XPATH, using="//*[@id=\"quantity_wanted_p\"]/a[1]")
    private WebElement cartRemoveOne;

    @FindBy(how = How.ID, using="group_1")
    private WebElement selectSize;

    @FindBy(how = How.ID, using="color_to_pick_list")
    private WebElement chooseColour;

    public ProductDescriptionPage(WebDriver webdriver) {
        this.driver = webdriver;
        PageFactory.initElements(this.driver,this);
    }

    public void selectColour(String colour){
        WebElement col = driver.findElement(By.name(colour));
        col.click();
    }

    public void incOrderCount() {
        cartAddOne.click();
    }

    public void decOrderCount() {
        cartRemoveOne.click();
    }

    public float getPrice() {
        String price = howMuch.getText().substring(1); // get rid of leading $ symbol
        return Float.parseFloat(price);
    }

    public void addItemToCart() {
        addToCart.click();
    }

    public void selectSize(String size) {
        Select sizeSelector = new Select(selectSize);
        sizeSelector.selectByVisibleText(size);
    }

}
