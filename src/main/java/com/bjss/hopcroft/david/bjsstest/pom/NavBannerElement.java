package com.bjss.hopcroft.david.bjsstest.pom;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class NavBannerElement {
    final static Logger logger = Logger.getLogger(QuickViewElement.class);
    WebDriver driver;

    @FindBy(how = How.CSS, using=".login")
    private WebElement bannerSignIn;

    @FindBy(how = How.CSS, using="#header > div.nav > div > div > nav > div:nth-child(2) > a")
    private WebElement bannerSignOut;

    public NavBannerElement(WebDriver webdriver) {
        this.driver = webdriver;
        PageFactory.initElements(this.driver,this);
    }

    public void gotoSignInPage() {
        bannerSignIn.click();
    }

    public void signOut() {
        bannerSignOut.click();
    }
}
