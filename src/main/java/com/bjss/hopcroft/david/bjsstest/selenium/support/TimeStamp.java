package com.bjss.hopcroft.david.bjsstest.selenium.support;

import java.sql.Timestamp;

public class TimeStamp {
    public static String epochDateTime() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return String.valueOf(timestamp.getTime());
    }

    public static String epochDateTimeShort() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return Long.toHexString(timestamp.getTime());
    }

}
