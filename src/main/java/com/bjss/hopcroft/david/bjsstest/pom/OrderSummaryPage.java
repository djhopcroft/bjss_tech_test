package com.bjss.hopcroft.david.bjsstest.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class OrderSummaryPage {
    WebDriver driver;

    @FindBy(how = How.CSS, using="#cart_navigation > button")
    private WebElement confirmTheOrder;

    public OrderSummaryPage(WebDriver wd) {
        this.driver = wd;
        PageFactory.initElements(this.driver,this);
    }

    public void clickConfirmOrder() {
        confirmTheOrder.click();
    }
}
