package com.bjss.hopcroft.david.bjsstest.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ShippingSelectionPage {
    WebDriver driver;

    @FindBy(how = How.NAME, using="processCarrier")
    private WebElement toCheckoutButton;

    @FindBy(how = How.NAME, using="cgv")
    private WebElement agreeTermsOfService;

    public ShippingSelectionPage(WebDriver wd) {
        this.driver = wd;
        PageFactory.initElements(this.driver,this);
    }

    public void agreeToTermsOfServce() {
        if (agreeTermsOfService.isSelected()!=true) {
            // Only Click if not already Selected
            agreeTermsOfService.click();
        }
    }

    public void clickProceedToCheckout() {
        toCheckoutButton.click();
    }
}
