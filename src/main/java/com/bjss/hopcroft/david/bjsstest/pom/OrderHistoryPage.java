package com.bjss.hopcroft.david.bjsstest.pom;

import com.bjss.hopcroft.david.bjsstest.Navigate;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.bjss.hopcroft.david.bjsstest.selenium.support.Waits.explicitWaitForElementLoad;

public class OrderHistoryPage {
    final static Logger LOG = Logger.getLogger(Navigate.class);
    WebDriver driver;

    List<WebElement> orderRows;
    List<WebElement> rowFields;

    @FindBy(how= How.ID, using="order-list")
    private WebElement orderList;

    @FindBy(how= How.NAME, using="id_product")
    private WebElement selectOrderItem;

    @FindBy(how= How.NAME, using="msgText")
    private WebElement addMessage;

    @FindBy(how= How.NAME, using="submitMessage")
    private WebElement sendMessage;

    @FindBy(how= How.CSS, using="#block-order-detail > div:nth-child(9) > table")
    private WebElement OrderMessages;

    public OrderHistoryPage(WebDriver wd) {
        this.driver = wd;
        PageFactory.initElements(this.driver,this);
    }

    public int getHowManyItem() {
        List<WebElement> bodyRows = orderList.findElements(By.cssSelector("tbody tr"));
        return bodyRows.size();
    }

    public void selectLatestOrder() {
        List<WebElement> findAllOrders = orderList.findElements(By.cssSelector("tbody tr"));
        for (WebElement order : findAllOrders) {
            String reference = order.findElements(By.cssSelector("td")).get(0).getText();
            String date = order.findElements(By.cssSelector("td")).get(1).getText();
            LOG.info("order " + reference + " made on " + date);
        }
        List<WebElement> orderRow = (List<WebElement>) findAllOrders.get(0).findElements(By.cssSelector("td"));
        LOG.info("Selecting Order: " + orderRow.get(0).getText());
        orderRow.get(0).click();
    }

    public void addMessageToOrder(String message) {
        explicitWaitForElementLoad(driver, selectOrderItem);
        Select product = new Select(selectOrderItem);
        product.selectByIndex(1);
        addMessage.sendKeys(message);
        sendMessage.submit();
    }

    public String getLatestMessageFromOrder() {
        String messageText = "";
        String latestMessage = "";
        String fromAndDate;
        Date dateOfMessage = new Date();
        Date newestDate = null;

        explicitWaitForElementLoad(driver, OrderMessages);

        List<WebElement> findAllMessages = OrderMessages.findElements(By.cssSelector("tbody tr"));
        for (WebElement message : findAllMessages) {
            fromAndDate = message.findElements(By.cssSelector("td")).get(0).getText();
            messageText = message.findElements(By.cssSelector("td")).get(1).getText();
            // String msgFrom = fromAndDate.split("\n")[0]; not needed ATM
            String dateOfMsg = fromAndDate.split("\n")[1];
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy kk:mm:ss");
            try {
                dateOfMessage = df.parse(dateOfMsg);
            } catch (ParseException e) {
                System.err.println(e);
            }
            if (newestDate == null) {
                //Seed values at first iteration
                newestDate = dateOfMessage;
                latestMessage = messageText;
            }
            if (dateOfMessage.after(newestDate)) {
                //Currently this should never trigger as date at top of table is the latest message
                newestDate = dateOfMessage;
                latestMessage = messageText;
            }
        }
        return latestMessage;
    }
}
