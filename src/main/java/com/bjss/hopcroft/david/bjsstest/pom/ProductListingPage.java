package com.bjss.hopcroft.david.bjsstest.pom;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import static com.bjss.hopcroft.david.bjsstest.selenium.support.Waits.explicitWaitForElementLoad;

public class ProductListingPage {
    final static Logger LOG = Logger.getLogger(QuickViewElement.class);

    WebDriver driver;
    public QuickViewElement qv;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using="Quick view")
    private WebElement quickview;

    @FindBy(how = How.CSS, using="#category > div.fancybox-overlay.fancybox-overlay-fixed > div > div")
    private WebElement quickViewContainer;

    public ProductListingPage(WebDriver webdriver) {
        this.driver = webdriver;
        PageFactory.initElements(this.driver,this);
    }

    public void openQuickViewFor(String description) {
        LOG.info("Open quick view for: '" + description + "'");
        // Locate the required Item on the page, First select the listing container.
        WebElement elem = driver.findElement(By.cssSelector("#center_column > ul"));
        // Then pick the product
        WebElement product = (elem.findElement(By.partialLinkText(description)));
        WebElement domContainer = product.findElement(By.xpath("../../../.."));
        //Wow, this is messy: "product" is the Text element for the product. But this is too far down the element to scroll
        // to and see the quick view button, So I use a relative xpath to navigate back out of parent elements until
        // I reach the container for the product, then the "quick view" element should become available if hover over it
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", domContainer);
        //Hover over item
        Actions builder = new Actions(driver);
        builder.moveToElement(product).perform();
        // Finally I can open the quick view element
        domContainer.findElement(By.partialLinkText("Quick view")).click();
        explicitWaitForElementLoad(driver, driver.findElement(By.cssSelector("#category > div.fancybox-overlay.fancybox-overlay-fixed > div > div")));
        qv = new QuickViewElement(driver);
    }
}
