package com.bjss.hopcroft.david.bjsstest.pom;

import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.HashMap;

import static com.bjss.hopcroft.david.bjsstest.selenium.support.Waits.explicitWaitForElementLoad;

public class CreateAccountPage {
    WebDriver driver;
    HashMap userData;

    @FindBy(how = How.ID, using="account-creation_form")
    private WebElement thisForm;

    @FindBy(how = How.ID, using="id_gender1")
    private WebElement mr;

    @FindBy(how = How.ID, using="id_gender2")
    private WebElement mrs;

    @FindBy(how = How.ID, using="customer_firstname")
    private WebElement firstName;

    @FindBy(how = How.ID, using="customer_lastname")
    private WebElement lastName;

    @FindBy(how = How.ID, using="email")
    private WebElement email;

    @FindBy(how = How.ID, using="passwd")
    private WebElement password;

    @FindBy(how = How.ID, using="days")
    private WebElement dayOfBirth;

    @FindBy(how = How.ID, using="months")
    private WebElement monthOfBirth;

    @FindBy(how = How.ID, using="years")
    private WebElement yearOfBirth;

    @FindBy(how = How.ID, using="newsletter")
    private WebElement newsletter;

    @FindBy(how = How.ID, using="optin")
    private WebElement offers;

    @FindBy(how = How.ID, using="firstname")
    private WebElement address_firstName;

    @FindBy(how = How.ID, using="lastname")
    private WebElement address_lastName;

    @FindBy(how = How.ID, using="address1")
    private WebElement address1;

    @FindBy(how = How.ID, using="city")
    private WebElement city;

    @FindBy(how = How.ID, using="id_state")
    private WebElement state;

    @FindBy(how = How.ID, using="postcode")
    private WebElement zip;

    @FindBy(how = How.ID, using="phone_mobile")
    private WebElement mobile_number;

    @FindBy(how = How.ID, using="alias")
    private WebElement alias;


    @FindBy(how = How.ID, using="submitAccount")
    private WebElement submitButton;

    public CreateAccountPage(WebDriver wd, HashMap info) {
        this.driver = wd;
        this.userData = info;
        PageFactory.initElements(this.driver,this);
    }

    public void fillData() {
        explicitWaitForElementLoad(this.driver, mr);
        if (userData.get("gender").toString().equals("Mr")) {
            mr.click();
        } else if (userData.get("gender").toString().equals("Mrs")) {
            mrs.click();
        } else {
            throw new ValueException("Gender must be 'Mr' or Mrs");
        }

        firstName.sendKeys(userData.get("name").toString());
        lastName.sendKeys(userData.get("surname").toString());
        email.clear();
        email.sendKeys(userData.get("email").toString());
        password.sendKeys(userData.get("passwd").toString());
        Select dob = new Select(dayOfBirth);
        dob.selectByValue(userData.get("dob").toString());
        Select mob = new Select(monthOfBirth);
        mob.selectByVisibleText(userData.get("mob").toString());
        Select yob = new Select(yearOfBirth);
        yob.selectByValue(userData.get("yob").toString());
        // Only click checkbox is current state is not what I want (XOR)
        // Unfortunately the data mapping passes in Object not typed values (I wanted Boolean)
        // So I treat the true fals as strings, sub-optimal...
        if (newsletter.isSelected() ^ (userData.get("news") == "true")) {
            newsletter.click();
        }
        if (offers.isSelected() ^ (userData.get("offer") == "true")) {
            offers.click();
        }
        address_firstName.clear();
        address_firstName.sendKeys(userData.get("name").toString());
        address_lastName.clear();
        address_lastName.sendKeys(userData.get("surname").toString());
        address1.sendKeys(userData.get("address").toString());
        city.sendKeys(userData.get("city").toString());
        Select select_state = new Select(state);
        select_state.selectByVisibleText(userData.get("state").toString());
        zip.sendKeys(userData.get("zip").toString());
        mobile_number.sendKeys(userData.get("mobile").toString());
        alias.clear();
        alias.sendKeys(userData.get("alias").toString());
    }

    /**
     * Keep the submit separate to the form filling incase I need to show form validation working
     * Incorrect values (malformed, too large, empty fields …)
     */
    public void submitForm() {
        thisForm.submit();
    }

    public void clickRegister() {
        submitButton.click();
    }
}
