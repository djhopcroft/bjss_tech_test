package com.bjss.hopcroft.david.bjsstest.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LandingPage {
    WebDriver driver;

    @FindBy(how = How.XPATH, using="//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a")
    private WebElement cart;

    public LandingPage(WebDriver webdriver) {
        this.driver = webdriver;
        PageFactory.initElements(this.driver,this);
    }

    public void clickGotoCart() {
        cart.click();
    }



}
