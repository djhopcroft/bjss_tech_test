package com.bjss.hopcroft.david.bjsstest.selenium.support;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

import static com.bjss.hopcroft.david.bjsstest.selenium.support.TimeStamp.epochDateTimeShort;


public class AutoScreenShotRule extends TestWatcher {
    private WebDriver driver;
    private static final Logger LOG = Logger.getLogger(AutoScreenShotRule.class.getName());

    public AutoScreenShotRule(WebDriver wd) {
        this.driver = wd;
    }

    @Override
    protected void failed(Throwable e, Description description) {
        String methodName = description.getMethodName();
        String filename = System.getProperty("user.appdata.evidence")
                + epochDateTimeShort().toUpperCase()
                + "-"
                + description.getTestClass().getSimpleName()
                + "-"
                + methodName
                + ".png";
        LOG.info("Test failed, screenshot attempting to save screen to: " + filename);

        try {
            File destination = new File(filename);
            FileUtils.copyFile(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE), destination);
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    @Override
    protected void finished(Description description) {
        LOG.info("Test failed, closing this driver instance");
        driver.quit();
    }
}
