package com.bjss.hopcroft.david.bjsstest.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import static com.bjss.hopcroft.david.bjsstest.selenium.support.Waits.explicitWaitForElementLoad;

public class AuthenticationPage {
    WebDriver driver;

    @FindBy(how=How.ID, using="email")
    private WebElement emailAddress;

    @FindBy(how=How.ID, using="email_create")
    private WebElement registrationEmailAddress;

    @FindBy(how = How.ID, using="passwd")
    private WebElement password;

    @FindBy(how = How.ID, using="SubmitLogin")
    private WebElement signin;

    @FindBy(how = How.ID, using="SubmitCreate")
    private WebElement createAccount;

    @FindBy(how=How.CSS, using="#center_column > h1")
    private WebElement myAccountHeading;


    public AuthenticationPage(WebDriver wd) {
        this.driver = wd;
        PageFactory.initElements(this.driver,this);
    }

    public void enterEmailAddress(String email) {
        emailAddress.clear();
        emailAddress.sendKeys(email);
    }

    public void enterPassword(String userPassword) {
        password.clear();
        password.sendKeys(userPassword);
    }

    public void clickSignIn() {
        signin.click();
        explicitWaitForElementLoad(this.driver, myAccountHeading);
    }

    public void registrationEmail(String email) {
        registrationEmailAddress.sendKeys(email);
    }

    public void createAccount() {
        createAccount.click();
    }
}
