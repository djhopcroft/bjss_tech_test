package com.bjss.hopcroft.david.bjsstest;

public class SystemConfiguration {
    public static final void systemSettings() {
        String version = "3.4.0";
        String os = "Mac";
        String basedir = "./src/main/resources/drivers/se/" + version +  "/" + os + "/";
        // Keep following in system properties so they can be accessed app wide.
        // In a real harness these would be kept in system properties file so they can be changed with out re-building code.
        System.setProperty("test.se.version", version);
        System.setProperty("test.sut.os", os);
        System.setProperty("test.data.drivers", basedir);
        System.setProperty("user.appdata.evidence", "./evidence/");
        // selenium properties
        System.setProperty("webdriver.gecko.driver", basedir + "Gecko_64/geckodriver");
        // Data for the test site
        System.setProperty("test.data.baseurl","http://automationpractice.com/");
        System.setProperty("test.data.username", "as@mailinator.com");
        System.setProperty("test.data.password", "BJSSTest");
    }
}
