package com.bjss.hopcroft.david.bjsstest.selenium.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waits {
    public Waits() {}

    public static void waitSeconds(float timeout) {
        try {
            Thread.sleep((int)(timeout*1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void explicitWaitForElementLoad(WebDriver driver, WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

}
