package com.bjss.hopcroft.david.bjsstest.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class PaymentPage {
    WebDriver driver;

    @FindBy(how = How.CSS, using="#HOOK_PAYMENT > div:nth-child(1) > div > p > a")
    private WebElement payByBankWire;

    @FindBy(how = How.CSS, using="#HOOK_PAYMENT > div:nth-child(2) > div > p > a")
    private WebElement payByCheque;

    public PaymentPage(WebDriver wd) {
        this.driver = wd;
        PageFactory.initElements(this.driver,this);
    }

    public void clickPayByBankWire() {
        payByBankWire.click();
    }

    public void clickPayByCheque() {
        payByCheque.click();
    }
}
