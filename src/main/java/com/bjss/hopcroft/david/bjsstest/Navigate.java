package com.bjss.hopcroft.david.bjsstest;

import com.bjss.hopcroft.david.bjsstest.pom.*;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.bjss.hopcroft.david.bjsstest.selenium.support.Waits.explicitWaitForElementLoad;

public class Navigate {
    final static Logger LOG = Logger.getLogger(Navigate.class);

    WebDriver wd;
    private ProductDescriptionPage pdp;
    private ProductListingPage plp;
    private CartPage cart;
    private AuthenticationPage signin;
    private AddressInfoPage addressInfo;
    private ShippingSelectionPage shippingInfo;
    private PaymentPage paymentMethod;
    private OrderSummaryPage orderSummary;
    private MyAccountMenuPage myAccountMenu;
    private OrderHistoryPage myOrders;

    private NavBannerElement navBanner;

    public Navigate(WebDriver driver) {
        this.wd = driver;
        pdp = new ProductDescriptionPage(wd);
        plp = new ProductListingPage(wd);
        cart = new CartPage(wd);
        signin = new AuthenticationPage(wd);
        addressInfo = new AddressInfoPage(wd);
        shippingInfo = new ShippingSelectionPage(wd);
        paymentMethod = new PaymentPage(wd);
        orderSummary = new OrderSummaryPage(wd);
        myAccountMenu = new MyAccountMenuPage(wd);
        myOrders = new OrderHistoryPage(wd);
        navBanner = new NavBannerElement(wd);
    }


    public float actionBrowseAndAddToCart(HashMap product){
        LOG.info("Adding " + product.toString() + " to the cart, from pdp");
        wd.navigate().to(product.get("pdp").toString());

        if (product.get("colour") != null) {
            pdp.selectColour(product.get("colour").toString());
        }
        if (product.get("size") != null) {
            pdp.selectSize(product.get("size").toString());
        }
        pdp.addItemToCart();
        return pdp.getPrice();
    }

    public double actionBrowsePlpAndAddToCart(HashMap product){
        double item_price;
        LOG.info("Adding " + product.toString() + " to the cart, from plp");
        wd.navigate().to(product.get("plp").toString());
        plp.openQuickViewFor(product.get("item").toString());
        if (product.get("colour") != null) {
            plp.qv.selectColour(product.get("colour").toString());
        }
        if (product.get("size") != null) {
            plp.qv.selectSize(product.get("size").toString());
        }
        item_price = plp.qv.getPrice();
        plp.qv.addItemToCart();
        return item_price;
    }

    public void gotoCart(){
        this.gotoHomePage();
        LandingPage hp = new LandingPage(wd);
        hp.clickGotoCart();
        explicitWaitForElementLoad(wd, wd.findElement(By.id("cart_title")));
    }

    public void gotoCheckout() {
        cart.clickProceedToCheckout();
    }

    // I am assuming that the Items are in the cart in the order they were added
    // TODO refactor this in to POM - cartPage.java
    public HashMap parseCheckout() {
        HashMap scrapedInfo = new HashMap();

        List<WebElement> tableRows;
        List<WebElement> rowFields;
        List<Object> contentInfo = new ArrayList<Object>();

        LOG.info("Items in cart: " + cart.getHowManyItem());
        WebElement wholeTable = this.wd.findElement(By.id("cart_summary"));
        tableRows = wholeTable.findElements(By.cssSelector("tbody tr"));

        String size, price;
        for (int rowIndex=0; rowIndex < cart.getHowManyItem();rowIndex++) {
            HashMap<String, String> orderLine = new HashMap<String, String>();
            size = "-";
            rowFields = tableRows.get(rowIndex).findElements(By.cssSelector("td"));
            for (String line : rowFields.get(1).getText().split("\n")) {
                if (line.startsWith("Color")) {
                    size = line.split(",")[1].split(":")[1].trim();
                }
            }
            price = rowFields.get(3).getText().split("\\s")[0].substring(1);
            orderLine.put("price", price);
            orderLine.put("size", size);
            contentInfo.add(orderLine);
        }
        scrapedInfo.put("items", contentInfo);
        tableRows = wholeTable.findElements(By.cssSelector("tfoot tr"));
        String totalProducts = tableRows.get(0).findElement(By.id("total_product")).getText().substring(1);
        scrapedInfo.put("totalProducts", totalProducts);
        //Hidden Row at (1) for gift wrap cost
        String shipping = tableRows.get(2).findElement(By.id("total_shipping")).getText().substring(1);
        scrapedInfo.put("totalShipping", shipping);
        //Hidden Row at (3) for gift voucher
        String total = tableRows.get(6).findElement(By.id("total_price")).getText().substring(1);
        scrapedInfo.put("totalPrice", total);
        LOG.info("Info scraped from the cart: " + scrapedInfo);
        return scrapedInfo;
    }

    public void payForItems(HashMap userInfo) {
        signin.enterEmailAddress(userInfo.get("login").toString());
        signin.enterPassword(userInfo.get("password").toString());
        signin.clickSignIn();
        addressInfo.clickProceedToCheckout();
        shippingInfo.agreeToTermsOfServce();
        shippingInfo.clickProceedToCheckout();
        paymentMethod.clickPayByBankWire();
        orderSummary.clickConfirmOrder();
    }

    public void gotoHomePage() {
        LOG.info("Home Page selected: " + System.getProperty("test.data.baseurl"));
        wd.get(System.getProperty("test.data.baseurl"));
    }

    public void gotoAccountCreation() {
        this.gotoHomePage();
        navBanner.gotoSignInPage();
        signin.registrationEmail(System.getProperty("test.data.username"));
        signin.createAccount();
    }


    public void createAccount(HashMap user) {
        CreateAccountPage account = new CreateAccountPage(wd, user);
        account.fillData();
        account.clickRegister();
    }

    public void loginToAccount(HashMap userInfo) {
        navBanner.gotoSignInPage();
        signin.enterEmailAddress(userInfo.get("login").toString());
        signin.enterPassword(userInfo.get("password").toString());
        signin.clickSignIn();
    }

    public void signOut() {
        navBanner.signOut();
    }

    public void gotoAccountOrderhistory() {
        myAccountMenu.gotoOrderHistory();
    }

    public void addMessageToLatestOrder(String message) {
        gotoAccountOrderhistory();
        LOG.info("I have "+ myOrders.getHowManyItem() + " Orders");
        myOrders.selectLatestOrder();
        myOrders.addMessageToOrder(message);
    }

    public String getLatestMessageFromOrder() {
        return myOrders.getLatestMessageFromOrder();
    }

}
