package com.bjss.hopcroft.david.bjsstest.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class MyAccountMenuPage {
    WebDriver driver;

    @FindBy(how= How.CSS, using="#header > div.nav > div > div > nav > div:nth-child(1) > a")
    private WebElement accountMainMenu;

    @FindBy(how= How.CSS, using="#center_column > div > div:nth-child(1) > ul > li:nth-child(1) > a > span")
    private WebElement orderHistoryAndDetails;


    public MyAccountMenuPage(WebDriver wd) {
        this.driver = wd;
        PageFactory.initElements(this.driver,this);
    }

    public void gotoOrderHistory() {
        orderHistoryAndDetails.click();
    }

    public void gotoAccountMenu() {
        accountMainMenu.click();
    }

}
