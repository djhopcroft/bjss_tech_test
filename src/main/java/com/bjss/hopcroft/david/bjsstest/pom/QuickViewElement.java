package com.bjss.hopcroft.david.bjsstest.pom;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import static com.bjss.hopcroft.david.bjsstest.selenium.support.Waits.explicitWaitForElementLoad;

public class QuickViewElement {
    final static Logger logger = Logger.getLogger(QuickViewElement.class);
    WebDriver driver;

    @FindBy(how = How.ID, using="add_to_cart")
    private WebElement addToCart;

    @FindBy(how = How.ID, using="quantity_wanted")
    private WebElement quantityToOrder;

    @FindBy(how = How.ID, using="our_price_display")
    private WebElement howMuch;

    @FindBy(how = How.XPATH, using="//*[@id=\"quantity_wanted_p\"]/a[1]/span/i")
    private WebElement cartAddOne;

    @FindBy(how = How.XPATH, using="//*[@id=\"quantity_wanted_p\"]/a[1]")
    private WebElement cartRemoveOne;

    @FindBy(how = How.ID, using="group_1")
    private WebElement selectSize;

    @FindBy(how = How.ID, using="color_to_pick_list")
    private WebElement chooseColour;

    @FindBy(how = How.ID, using="layer_cart")
    private WebElement popUpCart;

    // iFrame changes ID so I have to assume there will be only 1 frame
    @FindBy(how = How.TAG_NAME, using="iframe")
    private WebElement quickViewIFrame;

    public QuickViewElement(WebDriver webdriver) {
        this.driver = webdriver;
        PageFactory.initElements(this.driver,this);
    }

    public void selectColour(String colour){
        driver.switchTo().frame(quickViewIFrame);
        WebElement col = driver.findElement(By.name(colour));
        col.click();
        driver.switchTo().defaultContent();
    }

    public void incOrderCount() {
        driver.switchTo().frame(quickViewIFrame);
        cartAddOne.click();
        driver.switchTo().defaultContent();
    }

    public void decOrderCount() {
        driver.switchTo().frame(quickViewIFrame);
        cartRemoveOne.click();
        driver.switchTo().defaultContent();
    }

    public float getPrice() {
        driver.switchTo().frame(quickViewIFrame);
        String price = howMuch.getText().substring(1); // get rid of leading $ symbol
        driver.switchTo().defaultContent();
        return Float.parseFloat(price);
    }

    public void addItemToCart() {
        driver.switchTo().frame(quickViewIFrame);
        addToCart.click();
        driver.switchTo().defaultContent();
        explicitWaitForElementLoad(driver, popUpCart);
    }

    public void selectSize(String size) {
        driver.switchTo().frame(quickViewIFrame);
        Select sizeSelector = new Select(selectSize);
        sizeSelector.selectByVisibleText(size);
        driver.switchTo().defaultContent();
    }
}
