package com.bjss.hopcroft.david.bjsstest.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class CartPage {
    WebDriver driver;

    @FindBy(how= How.ID, using="cart_summary")
    private WebElement summaryTable;

    @FindBy(how= How.ID, using="total_product")
    private WebElement totalProducts;

    @FindBy(how= How.ID, using="total_shipping")
    private WebElement shippingCost;

    @FindBy(how= How.ID, using="total_price_container")
    private WebElement ammountToPay;

    @FindBy(how= How.XPATH, using="//*[@id=\"cart_summary\"]/tbody")
    private WebElement cartInventory;

    @FindBy(how=How.CSS, using="#center_column > p.cart_navigation.clearfix > a.button.btn.btn-default.standard-checkout.button-medium")
    private WebElement proceedToCheckout;

    //TODO refactor to this class
    public CartPage(WebDriver wd) {
        this.driver = wd;
        PageFactory.initElements(this.driver,this);
    }

    public int getHowManyItem() {
        List<WebElement> bodyRows = summaryTable.findElements(By.cssSelector("tbody tr"));
        return bodyRows.size();
    }

    public double getLineTotal(int lineNumber) {
        return 0.0;
    }

    public double getCalculatedItemTotal() {
        return 0.0;
    }

    public String getDescription() {
        return "";
    }

    public double getPayableTotal() {
        return 0.0;
    }

    public void clickProceedToCheckout() {
        proceedToCheckout.click();
    }

}
