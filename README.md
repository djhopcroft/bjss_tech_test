# Demonstration Selenium and API test harness

## Objective
I have tried to make this as simple as possible, because of the excellent XML and JSON processing I decided to 
use the SPOCK test framework as this supports Groovy syntax and spec style tests.
I have tried to make this as independent of external libraries as possible, But needed HTTPBuilder from the GroovyX 
package. However if I had used say Python I would still have brought in the "requests" package for the API testing.

## Project notes
Tests are split in to two modules on each for the API and website tests.  These would need refactoring down with more
granularity if this test suite was to be expanded further.

Project will use Maven for dependency management.

As mentioned above project will use SPOCK as the test frame work, underlying this is junit so the harness will present 
standard JUnit test reports.

### API
I spent a lot of time playing around with the API test code, normally I would use requests and python for this type of 
testing. But I was interested to see how this turned out. I think there is a lot of room for improvement BUT I am happy 
about the final results, at least for the time I have spent on it. Also the reqres.in site documentation is not accurate
about the response data. so I had a small detour looking for data in the respons that wasn't there. I needed to uses a 
browser rest client to confirm what I was seeing in the test cases.

Due to the nature of API testing I would also like this to be data driven, I would want to have a set of input data and 
for each input would like a list of hte things that should be checked.  Then all I would need would be a few test cases 
that could be reused, for many, many test cases.

### Web
For these tests I have used Selenuim 3.4.0 as I have all the driver versions matched to this version. all relevent files
are kept in the resources directory in the project.

I tend to prefer Property files and settings for the configuration but as this is a "quick" demo I have skipped this.

Also not included was a helper to auto configure the correct path's and settings for different browsers on different OSes

This code was written on a Mac, so be aware I have not test any thing works on Windows, nor are there any graceful checks 
for valid parameter setting i.e. Internet explorer on Mac OS.

I have created an extra test that will create a user, simply because I like to automate every thing and doing this gives
me control over the user creation process, using this test I can create as many users in the system as I need.  
This test is normally set to @Ignore and only called when I need it, so this will not create users every run.

#### Web test layout
The web tests have been split in to two tests, one is a Spec test and the other is a standard unit test to show this 
harness is interoperable with various types of testing methods. The Unit test is expected to fail. This fail triggers the 
screen shot.  The Image file can be found in the project root directory in a directory called ./evidences/ the files are 
time stamped to give a unique file name that won't be overwritten on subsiquent executions.  This file name also links
the picture to the failed test and suite.

## State of Submission
1. This code needs a lot of refactoring. html table parsing for example
2. The "Test 3" code is a bit of a cheat, but do take a automatic screenshot on fail. Sorry Just a lack of time.
3. There is a lot of messy code in this submission see point 1 above.  again ideally I would like a code review and tidy up time.
4. Test 2 step 2, this might need better wording unless I missed something.  The orders don't have data and time, but the 
Messages do. So this is where I placed the checking for date time.
5. In case you miss this the screenshots from Test 3 are found in a directory called "evidence" in the root of the project.
6. Test 4, I did not use the PUT method. The test only asked to check each of the CRUD actions and PUT would have been a duplication

 

